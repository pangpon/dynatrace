# 1 "c:\\users\\pon\\documents\\vugen\\scripts\\testroad\\\\combined_testroad.c"
# 1 "D:\\load\\include/lrun.h" 1
 
 












 











# 103 "D:\\load\\include/lrun.h"





















































		


		typedef unsigned size_t;
	
	
        
	

















	

 



















 
 
 
 
 


 
 
 
 
 
 














int     lr_start_transaction   (char * transaction_name);
int lr_start_sub_transaction          (char * transaction_name, char * trans_parent);
long lr_start_transaction_instance    (char * transaction_name, long parent_handle);
int   lr_start_cross_vuser_transaction		(char * transaction_name, char * trans_id_param); 



int     lr_end_transaction     (char * transaction_name, int status);
int lr_end_sub_transaction            (char * transaction_name, int status);
int lr_end_transaction_instance       (long transaction, int status);
int   lr_end_cross_vuser_transaction	(char * transaction_name, char * trans_id_param, int status);


 
typedef char* lr_uuid_t;
 



lr_uuid_t lr_generate_uuid();

 


int lr_generate_uuid_free(lr_uuid_t uuid);

 



int lr_generate_uuid_on_buf(lr_uuid_t buf);

   
# 273 "D:\\load\\include/lrun.h"
int lr_start_distributed_transaction  (char * transaction_name, lr_uuid_t correlator, long timeout  );

   







int lr_end_distributed_transaction  (lr_uuid_t correlator, int status);


double lr_stop_transaction            (char * transaction_name);
double lr_stop_transaction_instance   (long parent_handle);


void lr_resume_transaction           (char * trans_name);
void lr_resume_transaction_instance  (long trans_handle);


int lr_update_transaction            (const char *trans_name);


 
void lr_wasted_time(long time);


 
int lr_set_transaction(const char *name, double duration, int status);
 
long lr_set_transaction_instance(const char *name, double duration, int status, long parent_handle);


int   lr_user_data_point                      (char *, double);
long lr_user_data_point_instance                   (char *, double, long);
 



int lr_user_data_point_ex(const char *dp_name, double value, int log_flag);
long lr_user_data_point_instance_ex(const char *dp_name, double value, long parent_handle, int log_flag);


int lr_transaction_add_info      (const char *trans_name, char *info);
int lr_transaction_instance_add_info   (long trans_handle, char *info);
int lr_dpoint_add_info           (const char *dpoint_name, char *info);
int lr_dpoint_instance_add_info        (long dpoint_handle, char *info);


double lr_get_transaction_duration       (char * trans_name);
double lr_get_trans_instance_duration    (long trans_handle);
double lr_get_transaction_think_time     (char * trans_name);
double lr_get_trans_instance_think_time  (long trans_handle);
double lr_get_transaction_wasted_time    (char * trans_name);
double lr_get_trans_instance_wasted_time (long trans_handle);
int    lr_get_transaction_status		 (char * trans_name);
int	   lr_get_trans_instance_status		 (long trans_handle);

 



int lr_set_transaction_status(int status);

 



int lr_set_transaction_status_by_name(int status, const char *trans_name);
int lr_set_transaction_instance_status(int status, long trans_handle);


typedef void* merc_timer_handle_t;
 

merc_timer_handle_t lr_start_timer();
double lr_end_timer(merc_timer_handle_t timer_handle);


 
 
 
 
 
 











 



int   lr_rendezvous  (char * rendezvous_name);
 




int   lr_rendezvous_ex (char * rendezvous_name);



 
 
 
 
 
char *lr_get_vuser_ip (void);
void   lr_whoami (int *vuser_id, char ** sgroup, int *scid);
char *	  lr_get_host_name (void);
char *	  lr_get_master_host_name (void);

 
long     lr_get_attrib_long	(char * attr_name);
char *   lr_get_attrib_string	(char * attr_name);
double   lr_get_attrib_double      (char * attr_name);

char * lr_paramarr_idx(const char * paramArrayName, unsigned int index);
char * lr_paramarr_random(const char * paramArrayName);
int    lr_paramarr_len(const char * paramArrayName);

int	lr_param_unique(const char * paramName);
int lr_param_sprintf(const char * paramName, const char * format, ...);


 
 
static void *ci_this_context = 0;






 








void lr_continue_on_error (int lr_continue);
char *   lr_unmask (const char *EncodedString);
char *   lr_decrypt (const char *EncodedString);


 
 
 
 
 
 



 







 















void   lr_abort (void);
void lr_exit(int exit_option, int exit_status);
void lr_abort_ex (unsigned long flags);

void   lr_peek_events (void);


 
 
 
 
 


void   lr_think_time (double secs);

 


void lr_force_think_time (double secs);


 
 
 
 
 



















int   lr_msg (char * fmt, ...);
int   lr_debug_message (unsigned int msg_class,
									    char * format,
										...);
# 513 "D:\\load\\include/lrun.h"
void   lr_new_prefix (int type,
                                 char * filename,
                                 int line);
# 516 "D:\\load\\include/lrun.h"
int   lr_log_message (char * fmt, ...);
int   lr_message (char * fmt, ...);
int   lr_error_message (char * fmt, ...);
int   lr_output_message (char * fmt, ...);
int   lr_vuser_status_message (char * fmt, ...);
int   lr_error_message_without_fileline (char * fmt, ...);
int   lr_fail_trans_with_error (char * fmt, ...);

 
 
 
 
 
# 540 "D:\\load\\include/lrun.h"

 
 
 
 
 





int   lr_next_row ( char * table);
int lr_advance_param ( char * param);



														  
														  

														  
														  

													      
 


char *   lr_eval_string (char * str);
int   lr_eval_string_ext (const char *in_str,
                                     unsigned long const in_len,
                                     char ** const out_str,
                                     unsigned long * const out_len,
                                     unsigned long const options,
                                     const char *file,
								     long const line);
# 574 "D:\\load\\include/lrun.h"
void   lr_eval_string_ext_free (char * * pstr);

 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
int lr_param_increment (char * dst_name,
                              char * src_name);
# 597 "D:\\load\\include/lrun.h"













											  
											  

											  
											  
											  

int	  lr_save_var (char *              param_val,
							  unsigned long const param_val_len,
							  unsigned long const options,
							  char *			  param_name);
# 621 "D:\\load\\include/lrun.h"
int   lr_save_string (const char * param_val, const char * param_name);



int   lr_set_custom_error_message (const char * param_val, ...);

int   lr_remove_custom_error_message ();


int   lr_free_parameter (const char * param_name);
int   lr_save_int (const int param_val, const char * param_name);
int   lr_save_timestamp (const char * tmstampParam, ...);
int   lr_save_param_regexp (const char *bufferToScan, unsigned int bufSize, ...);

int   lr_convert_double_to_integer (const char *source_param_name, const char * target_param_name);
int   lr_convert_double_to_double (const char *source_param_name, const char *format_string, const char * target_param_name);

 
 
 
 
 
 
# 700 "D:\\load\\include/lrun.h"
void   lr_save_datetime (const char *format, int offset, const char *name);









 











 
 
 
 
 






 



char * lr_error_context_get_entry (char * key);

 



long   lr_error_context_get_error_id (void);


 
 
 

int lr_table_get_rows_num (char * param_name);

int lr_table_get_cols_num (char * param_name);

char * lr_table_get_cell_by_col_index (char * param_name, int row, int col);

char * lr_table_get_cell_by_col_name (char * param_name, int row, const char* col_name);

int lr_table_get_column_name_by_index (char * param_name, int col, 
											char * * const col_name,
											size_t * col_name_len);
# 761 "D:\\load\\include/lrun.h"

int lr_table_get_column_name_by_index_free (char * col_name);

 
 
 
 
# 776 "D:\\load\\include/lrun.h"
int   lr_zip (const char* param1, const char* param2);
int   lr_unzip (const char* param1, const char* param2);

 
 
 
 
 
 
 
 

 
 
 
 
 
 
int   lr_param_substit (char * file,
                                   int const line,
                                   char * in_str,
                                   size_t const in_len,
                                   char * * const out_str,
                                   size_t * const out_len);
# 800 "D:\\load\\include/lrun.h"
void   lr_param_substit_free (char * * pstr);


 
# 812 "D:\\load\\include/lrun.h"





char *   lrfnc_eval_string (char * str,
                                      char * file_name,
                                      long const line_num);
# 820 "D:\\load\\include/lrun.h"


int   lrfnc_save_string ( const char * param_val,
                                     const char * param_name,
                                     const char * file_name,
                                     long const line_num);
# 826 "D:\\load\\include/lrun.h"

int   lrfnc_free_parameter (const char * param_name );







typedef struct _lr_timestamp_param
{
	int iDigits;
}lr_timestamp_param;

extern const lr_timestamp_param default_timestamp_param;

int   lrfnc_save_timestamp (const char * param_name, const lr_timestamp_param* time_param);

int lr_save_searched_string(char * buffer, long buf_size, unsigned int occurrence,
			    char * search_string, int offset, unsigned int param_val_len, 
			    char * param_name);

 
char *   lr_string (char * str);

 
# 929 "D:\\load\\include/lrun.h"

int   lr_save_value (char * param_val,
                                unsigned long const param_val_len,
                                unsigned long const options,
                                char * param_name,
                                char * file_name,
                                long const line_num);
# 936 "D:\\load\\include/lrun.h"


 
 
 
 
 











int   lr_printf (char * fmt, ...);
 
int   lr_set_debug_message (unsigned int msg_class,
                                       unsigned int swtch);
# 958 "D:\\load\\include/lrun.h"
unsigned int   lr_get_debug_message (void);


 
 
 
 
 

void   lr_double_think_time ( double secs);
void   lr_usleep (long);


 
 
 
 
 
 




int *   lr_localtime (long offset);


int   lr_send_port (long port);


# 1034 "D:\\load\\include/lrun.h"



struct _lr_declare_identifier{
	char signature[24];
	char value[128];
};

int   lr_pt_abort (void);

void vuser_declaration (void);






# 1063 "D:\\load\\include/lrun.h"


# 1075 "D:\\load\\include/lrun.h"
















 
 
 
 
 







int    _lr_declare_transaction   (char * transaction_name);


 
 
 
 
 







int   _lr_declare_rendezvous  (char * rendezvous_name);

 
 
 
 
 


typedef int PVCI;






typedef int VTCERR;









PVCI   vtc_connect(char * servername, int portnum, int options);
VTCERR   vtc_disconnect(PVCI pvci);
VTCERR   vtc_get_last_error(PVCI pvci);
VTCERR   vtc_query_column(PVCI pvci, char * columnName, int columnIndex, char * *outvalue);
VTCERR   vtc_query_row(PVCI pvci, int rowIndex, char * **outcolumns, char * **outvalues);
VTCERR   vtc_send_message(PVCI pvci, char * column, char * message, unsigned short *outRc);
VTCERR   vtc_send_if_unique(PVCI pvci, char * column, char * message, unsigned short *outRc);
VTCERR   vtc_send_row1(PVCI pvci, char * columnNames, char * messages, char * delimiter, unsigned char sendflag, unsigned short *outUpdates);
VTCERR   vtc_update_message(PVCI pvci, char * column, int index , char * message, unsigned short *outRc);
VTCERR   vtc_update_message_ifequals(PVCI pvci, char * columnName, int index,	char * message, char * ifmessage, unsigned short 	*outRc);
VTCERR   vtc_update_row1(PVCI pvci, char * columnNames, int index , char * messages, char * delimiter, unsigned short *outUpdates);
VTCERR   vtc_retrieve_message(PVCI pvci, char * column, char * *outvalue);
VTCERR   vtc_retrieve_messages1(PVCI pvci, char * columnNames, char * delimiter, char * **outvalues);
VTCERR   vtc_retrieve_row(PVCI pvci, char * **outcolumns, char * **outvalues);
VTCERR   vtc_rotate_message(PVCI pvci, char * column, char * *outvalue, unsigned char sendflag);
VTCERR   vtc_rotate_messages1(PVCI pvci, char * columnNames, char * delimiter, char * **outvalues, unsigned char sendflag);
VTCERR   vtc_rotate_row(PVCI pvci, char * **outcolumns, char * **outvalues, unsigned char sendflag);
VTCERR   vtc_increment(PVCI pvci, char * column, int index , int incrValue, int *outValue);
VTCERR   vtc_clear_message(PVCI pvci, char * column, int index , unsigned short *outRc);
VTCERR   vtc_clear_column(PVCI pvci, char * column, unsigned short *outRc);
VTCERR   vtc_ensure_index(PVCI pvci, char * column, unsigned short *outRc);
VTCERR   vtc_drop_index(PVCI pvci, char * column, unsigned short *outRc);
VTCERR   vtc_clear_row(PVCI pvci, int rowIndex, unsigned short *outRc);
VTCERR   vtc_create_column(PVCI pvci, char * column,unsigned short *outRc);
VTCERR   vtc_column_size(PVCI pvci, char * column, int *size);
void   vtc_free(char * msg);
void   vtc_free_list(char * *msglist);

VTCERR   lrvtc_connect(char * servername, int portnum, int options);
VTCERR   lrvtc_connect_ex(char * vtc_first_param, ...);
VTCERR   lrvtc_connect_ex_no_ellipsis(const char *vtc_first_param, char ** arguments, int argCount);
VTCERR   lrvtc_disconnect();
VTCERR   lrvtc_query_column(char * columnName, int columnIndex);
VTCERR   lrvtc_query_row(int columnIndex);
VTCERR   lrvtc_send_message(char * columnName, char * message);
VTCERR   lrvtc_send_if_unique(char * columnName, char * message);
VTCERR   lrvtc_send_row1(char * columnNames, char * messages, char * delimiter, unsigned char sendflag);
VTCERR   lrvtc_update_message(char * columnName, int index , char * message);
VTCERR   lrvtc_update_message_ifequals(char * columnName, int index, char * message, char * ifmessage);
VTCERR   lrvtc_update_row1(char * columnNames, int index , char * messages, char * delimiter);
VTCERR   lrvtc_retrieve_message(char * columnName);
VTCERR   lrvtc_retrieve_messages1(char * columnNames, char * delimiter);
VTCERR   lrvtc_retrieve_row();
VTCERR   lrvtc_rotate_message(char * columnName, unsigned char sendflag);
VTCERR   lrvtc_rotate_messages1(char * columnNames, char * delimiter, unsigned char sendflag);
VTCERR   lrvtc_rotate_row(unsigned char sendflag);
VTCERR   lrvtc_increment(char * columnName, int index , int incrValue);
VTCERR   lrvtc_noop();
VTCERR   lrvtc_clear_message(char * columnName, int index);
VTCERR   lrvtc_clear_column(char * columnName); 
VTCERR   lrvtc_ensure_index(char * columnName); 
VTCERR   lrvtc_drop_index(char * columnName); 
VTCERR   lrvtc_clear_row(int rowIndex);
VTCERR   lrvtc_create_column(char * columnName);
VTCERR   lrvtc_column_size(char * columnName);



 
 
 
 
 

 
int lr_enable_ip_spoofing();
int lr_disable_ip_spoofing();


 




int lr_convert_string_encoding(char * sourceString, char * fromEncoding, char * toEncoding, char * paramName);
int lr_read_file(const char *filename, const char *outputParam, int continueOnError);

int lr_get_char_count(const char * string);


 
int lr_db_connect (char * pFirstArg, ...);
int lr_db_disconnect (char * pFirstArg,	...);
int lr_db_executeSQLStatement (char * pFirstArg, ...);
int lr_db_dataset_action(char * pFirstArg, ...);
int lr_checkpoint(char * pFirstArg,	...);
int lr_db_getvalue(char * pFirstArg, ...);







 
 



















# 1 "c:\\users\\pon\\documents\\vugen\\scripts\\testroad\\\\combined_testroad.c" 2

# 1 "D:\\load\\include/SharedParameter.h" 1



 
 
 
 
# 100 "D:\\load\\include/SharedParameter.h"






typedef int PVCI2;






typedef int VTCERR2;


 
 
 

 
extern PVCI2    vtc_connect(char *servername, int portnum, int options);
extern VTCERR2  vtc_disconnect(PVCI2 pvci);
extern VTCERR2  vtc_get_last_error(PVCI2 pvci);

 
extern VTCERR2  vtc_query_column(PVCI2 pvci, char *columnName, int columnIndex, char **outvalue);
extern VTCERR2  vtc_query_row(PVCI2 pvci, int columnIndex, char ***outcolumns, char ***outvalues);
extern VTCERR2  vtc_send_message(PVCI2 pvci, char *column, char *message, unsigned short *outRc);
extern VTCERR2  vtc_send_if_unique(PVCI2 pvci, char *column, char *message, unsigned short *outRc);
extern VTCERR2  vtc_send_row1(PVCI2 pvci, char *columnNames, char *messages, char *delimiter,  unsigned char sendflag, unsigned short *outUpdates);
extern VTCERR2  vtc_update_message(PVCI2 pvci, char *column, int index , char *message, unsigned short *outRc);
extern VTCERR2  vtc_update_message_ifequals(PVCI2 pvci, char	*columnName, int index,	char *message, char	*ifmessage,	unsigned short 	*outRc);
extern VTCERR2  vtc_update_row1(PVCI2 pvci, char *columnNames, int index , char *messages, char *delimiter, unsigned short *outUpdates);
extern VTCERR2  vtc_retrieve_message(PVCI2 pvci, char *column, char **outvalue);
extern VTCERR2  vtc_retrieve_messages1(PVCI2 pvci, char *columnNames, char *delimiter, char ***outvalues);
extern VTCERR2  vtc_retrieve_row(PVCI2 pvci, char ***outcolumns, char ***outvalues);
extern VTCERR2  vtc_rotate_message(PVCI2 pvci, char *column, char **outvalue, unsigned char sendflag);
extern VTCERR2  vtc_rotate_messages1(PVCI2 pvci, char *columnNames, char *delimiter, char ***outvalues, unsigned char sendflag);
extern VTCERR2  vtc_rotate_row(PVCI2 pvci, char ***outcolumns, char ***outvalues, unsigned char sendflag);
extern VTCERR2	vtc_increment(PVCI2 pvci, char *column, int index , int incrValue, int *outValue);
extern VTCERR2  vtc_clear_message(PVCI2 pvci, char *column, int index , unsigned short *outRc);
extern VTCERR2  vtc_clear_column(PVCI2 pvci, char *column, unsigned short *outRc);

extern VTCERR2  vtc_clear_row(PVCI2 pvci, int rowIndex, unsigned short *outRc);

extern VTCERR2  vtc_create_column(PVCI2 pvci, char *column,unsigned short *outRc);
extern VTCERR2  vtc_column_size(PVCI2 pvci, char *column, int *size);
extern VTCERR2  vtc_ensure_index(PVCI2 pvci, char *column, unsigned short *outRc);
extern VTCERR2  vtc_drop_index(PVCI2 pvci, char *column, unsigned short *outRc);

extern VTCERR2  vtc_noop(PVCI2 pvci);

 
extern void vtc_free(char *msg);
extern void vtc_free_list(char **msglist);

 


 




 




















 




 
 
 

extern VTCERR2  lrvtc_connect(char *servername, int portnum, int options);
 
 
extern VTCERR2  lrvtc_disconnect();
extern VTCERR2  lrvtc_query_column(char *columnName, int columnIndex);
extern VTCERR2  lrvtc_query_row(int columnIndex);
extern VTCERR2  lrvtc_send_message(char *columnName, char *message);
extern VTCERR2  lrvtc_send_if_unique(char *columnName, char *message);
extern VTCERR2  lrvtc_send_row1(char *columnNames, char *messages, char *delimiter,  unsigned char sendflag);
extern VTCERR2  lrvtc_update_message(char *columnName, int index , char *message);
extern VTCERR2  lrvtc_update_message_ifequals(char *columnName, int index, char 	*message, char *ifmessage);
extern VTCERR2  lrvtc_update_row1(char *columnNames, int index , char *messages, char *delimiter);
extern VTCERR2  lrvtc_retrieve_message(char *columnName);
extern VTCERR2  lrvtc_retrieve_messages1(char *columnNames, char *delimiter);
extern VTCERR2  lrvtc_retrieve_row();
extern VTCERR2  lrvtc_rotate_message(char *columnName, unsigned char sendflag);
extern VTCERR2  lrvtc_rotate_messages1(char *columnNames, char *delimiter, unsigned char sendflag);
extern VTCERR2  lrvtc_rotate_row(unsigned char sendflag);
extern VTCERR2  lrvtc_increment(char *columnName, int index , int incrValue);
extern VTCERR2  lrvtc_clear_message(char *columnName, int index);
extern VTCERR2  lrvtc_clear_column(char *columnName);
extern VTCERR2  lrvtc_clear_row(int rowIndex);
extern VTCERR2  lrvtc_create_column(char *columnName);
extern VTCERR2  lrvtc_column_size(char *columnName);
extern VTCERR2  lrvtc_ensure_index(char *columnName);
extern VTCERR2  lrvtc_drop_index(char *columnName);

extern VTCERR2  lrvtc_noop();

 
 
 

                               


 
 
 





















# 2 "c:\\users\\pon\\documents\\vugen\\scripts\\testroad\\\\combined_testroad.c" 2

# 1 "globals.h" 1



 
 

# 1 "D:\\load\\include/web_api.h" 1







# 1 "D:\\load\\include/as_web.h" 1



























































 




 



 











 





















 
 
 

  int
	web_add_filter(
		const char *		mpszArg,
		...
	);									 
										 
										 
										 
										 
										 
										 
										 
										 
										 
										 
										 
										 
										 
										 
										 
										 

  int
	web_add_auto_filter(
		const char *		mpszArg,
		...
	);									 
										 
										 
										 
										 
										 
										 
										 
										 
										 
										 
										 
										 
										 
										 
										 
										 
										 
	
  int
	web_add_auto_header(
		const char *		mpszHeader,
		const char *		mpszValue);

  int
	web_add_header(
		const char *		mpszHeader,
		const char *		mpszValue);
  int
	web_add_cookie(
		const char *		mpszCookie);
  int
	web_cleanup_auto_headers(void);
  int
	web_cleanup_cookies(void);
  int
	web_concurrent_end(
		const char * const	mpszReserved,
										 
		...								 
	);
  int
	web_concurrent_start(
		const char * const	mpszConcurrentGroupName,
										 
										 
		...								 
										 
	);
  int
	web_create_html_param(
		const char *		mpszParamName,
		const char *		mpszLeftDelim,
		const char *		mpszRightDelim);
  int
	web_create_html_param_ex(
		const char *		mpszParamName,
		const char *		mpszLeftDelim,
		const char *		mpszRightDelim,
		const char *		mpszNum);
  int
	web_custom_request(
		const char *		mpszReqestName,
		...);							 
										 
										 
										 
										 
										 
										 
										 
										 
										 
										 
										 
										 
										 
										 
										 
										 
										 
										 
										 
										 
										 
										 
										 
										 
  int
	spdy_custom_request(
		const char *		mpszReqestName,
		...);							 
										 
										 
										 
										 
										 
										 
										 
										 
										 
										 
										 
										 
										 
										 
										 
										 
										 
										 
										 
										 
										 
										 
										 
										 
										 
  int
	web_disable_keep_alive(void);
  int
	web_enable_keep_alive(void);
  int
	web_find(
		const char *		mpszStepName,
		...);							 
										 
										 
										 
										 
										 
										 
										 
										 
										 
  int
	web_get_int_property(
		const int			miHttpInfoType);
  int
	web_image(
		const char *		mpszStepName,
		...);							 
										 
										 
										 
										 
										 
										 
										 
										 
										 
										 
										 
										 
										 
										 
  int
	web_image_check(
		const char *		mpszName,
		...);
  int
	web_java_check(
		const char *		mpszName,
		...);
  int
	web_link(
		const char *		mpszStepName,
		...);							 
										 
										 
										 
										 
										 
										 
										 
										 
										 
										 
										 
										 
										 

	
  int
	web_global_verification(
		const char *		mpszArg1,
		...);							 
										 
										 
										 
										 
										 
  int
	web_reg_find(
		const char *		mpszArg1,
		...);							 
										 
										 
										 
										 
										 
										 
										 
				
  int
	web_reg_save_param(
		const char *		mpszParamName,
		...);							 
										 
										 
										 
										 
										 
										 

  int
	web_convert_param(
		const char * 		mpszParamName, 
										 
		...);							 
										 
										 


										 

										 
  int
	web_remove_auto_filter(
		const char *		mpszArg,
		...
	);									 
										 
				
  int
	web_remove_auto_header(
		const char *		mpszHeaderName,
		...);							 
										 



  int
	web_remove_cookie(
		const char *		mpszCookie);

  int
	web_save_header(
		const char *		mpszType,	 
		const char *		mpszName);	 
  int
	web_set_certificate(
		const char *		mpszIndex);
  int
	web_set_certificate_ex(
		const char *		mpszArg1,
		...);							 
										 
										 
										 
										 
										 
										 
										 
										 
										 
										 
										 
  int
	web_set_connections_limit(
		const char *		mpszLimit);
  int
	web_set_max_html_param_len(
		const char *		mpszLen);
  int
	web_set_max_retries(
		const char *		mpszMaxRetries);
  int
	web_set_proxy(
		const char *		mpszProxyHost);
  int
	web_set_pac(
		const char *		mpszPacUrl);
  int
	web_set_proxy_bypass(
		const char *		mpszBypass);
  int
	web_set_secure_proxy(
		const char *		mpszProxyHost);
  int
	web_set_sockets_option(
		const char *		mpszOptionID,
		const char *		mpszOptionValue
	);
  int
	web_set_option(
		const char *		mpszOptionID,
		const char *		mpszOptionValue,
		...								 
	);
  int
	web_set_timeout(
		const char *		mpszWhat,
		const char *		mpszTimeout);
  int
	web_set_user(
		const char *		mpszUserName,
		const char *		mpszPwd,
		const char *		mpszHost);

  int
	web_sjis_to_euc_param(
		const char *		mpszParamName,
										 
		const char *		mpszParamValSjis);
										 

  int
	web_submit_data(
		const char *		mpszStepName,
		...);							 
										 
										 
										 
										 
										 
										 
										 
										 
										 
										 
										 
										 
										 
										 
										 
										 
										 
										 
										 
										 
										 
										 
										 
										 
										 
										 
										 
										 
  int
	spdy_submit_data(
		const char *		mpszStepName,
		...);							 
										 
										 
										 
										 
										 
										 
										 
										 
										 
										 
										 
										 
										 
										 
										 
										 
										 
										 
										 
										 
										 
										 
										 
										 
										 
										 
										 
										 
										 

  int
	web_submit_form(
		const char *		mpszStepName,
		...);							 
										 
										 
										 
										 
										 
										 
										 
										 
										  
										 
										 
										 
										 
										 
										  
										 
										 
										 
										 
										 
										 
										 
										  
										 
										 
										 
										 
										 
										  
										 
										 
										 
										 
										 
										 
										 
										 
										 
										 
										 
  int
	web_url(
		const char *		mpszUrlName,
		...);							 
										 
										 
										 
										 
										 
										 
										 
										 
										 
										 
										 
										 
										 
										 
										 
										 

  int
	spdy_url(
		const char *		mpszUrlName,
		...);							 
										 
										 
										 
										 
										 
										 
										 
										 
										 
										 
										 
										 
										 
										 
										 
										 
										 

  int 
	web_set_proxy_bypass_local(
		const char * mpszNoLocal
		);

  int 
	web_cache_cleanup(void);

  int
	web_create_html_query(
		const char* mpszStartQuery,
		...);							 
										 
										 
										 
										 
										 
										 
										 
										 
										 
										 
										 
										 
										 
										 
										 
										 
										 
										 
										 

  int 
	web_create_radio_button_param(
		const char *NameFiled,
		const char *NameAndVal,
		const char *ParamName
		);

  int
	web_convert_from_formatted(
		const char * mpszArg1,
		...);							 
										 
										 
										 
										 
										 
										
  int
	web_convert_to_formatted(
		const char * mpszArg1,
		...);							 
										 
										 
										 
										 
										 

  int
	web_reg_save_param_ex(
		const char * mpszParamName,
		...);							 
										 
										 
										 
										 
										 
										 
										 
										 
										 
										 
										 
										 
										 

  int
	web_reg_save_param_xpath(
		const char * mpszParamName,
		...);							
										 
										 
										 
										 
										 
										 
										 
										 
										 
										 
										 
										 

  int
	web_reg_save_param_json(
		const char * mpszParamName,
		...);							
										 
										 
										 
										 
										 
										 
										 
										 
										 
										 
										 

  int
	web_reg_save_param_regexp(
		 const char * mpszParamName,
		 ...);							
										 
										 
										 
										 
										 
										 
										 
										 
										 
										 
										 

  int
	web_reg_save_param_attrib(
		const char * mpszParamName,
		...);
										 
										 
										 
										 
										 
										 
										 		
										 
										 
										 
										 
										 
										 
										 
										 

  int
	web_js_run(
		const char * mpszCode,
		...);							
										 
										 
										 
										 
										 
										 
										 
										 
										 

  int
	web_js_reset(void);

  int
	web_convert_date_param(
		const char * 		mpszParamName,
		...);










# 789 "D:\\load\\include/as_web.h"


# 802 "D:\\load\\include/as_web.h"



























# 840 "D:\\load\\include/as_web.h"

 
 
 


  int
	FormSubmit(
		const char *		mpszFormName,
		...);
  int
	InitWebVuser(void);
  int
	SetUser(
		const char *		mpszUserName,
		const char *		mpszPwd,
		const char *		mpszHost);
  int
	TerminateWebVuser(void);
  int
	URL(
		const char *		mpszUrlName);
























# 908 "D:\\load\\include/as_web.h"


  int
	web_rest(
		const char *		mpszReqestName,
		...);							 
										 
										 
										 
										 

  int
web_stream_open(
	const char *		mpszArg1,
	...
);
  int
	web_stream_wait(
		const char *		mpszArg1,
		...
	);

  int
	web_stream_close(
		const char *		mpszArg1,
		...
	);

  int
web_stream_play(
	const char *		mpszArg1,
	...
	);

  int
web_stream_pause(
	const char *		mpszArg1,
	...
	);

  int
web_stream_seek(
	const char *		mpszArg1,
	...
	);

  int
web_stream_get_param_int(
	const char*			mpszStreamID,
	const int			miStateType
	);

  double
web_stream_get_param_double(
	const char*			mpszStreamID,
	const int			miStateType
	);

  int
web_stream_get_param_string(
	const char*			mpszStreamID,
	const int			miStateType,
	const char*			mpszParameterName
	);

  int
web_stream_set_param_int(
	const char*			mpszStreamID,
	const int			miStateType,
	const int			miStateValue
	);

  int
web_stream_set_param_double(
	const char*			mpszStreamID,
	const int			miStateType,
	const double		mdfStateValue
	);

  int
web_stream_set_custom_mpd(
	const char*			mpszStreamID,
	const char*			aMpdBuf
	);

 
 
 






# 9 "D:\\load\\include/web_api.h" 2

















 







 















  int
	web_reg_add_cookie(
		const char *		mpszCookie,
		...);							 
										 

  int
	web_report_data_point(
		const char *		mpszEventType,
		const char *		mpszEventName,
		const char *		mpszDataPointName,
		const char *		mpszLAST);	 
										 
										 
										 

  int
	web_text_link(
		const char *		mpszStepName,
		...);

  int
	web_element(
		const char *		mpszStepName,
		...);

  int
	web_image_link(
		const char *		mpszStepName,
		...);

  int
	web_static_image(
		const char *		mpszStepName,
		...);

  int
	web_image_submit(
		const char *		mpszStepName,
		...);

  int
	web_button(
		const char *		mpszStepName,
		...);

  int
	web_edit_field(
		const char *		mpszStepName,
		...);

  int
	web_radio_group(
		const char *		mpszStepName,
		...);

  int
	web_check_box(
		const char *		mpszStepName,
		...);

  int
	web_list(
		const char *		mpszStepName,
		...);

  int
	web_text_area(
		const char *		mpszStepName,
		...);

  int
	web_map_area(
		const char *		mpszStepName,
		...);

  int
	web_eval_java_script(
		const char *		mpszStepName,
		...);

  int
	web_reg_dialog(
		const char *		mpszArg1,
		...);

  int
	web_reg_cross_step_download(
		const char *		mpszArg1,
		...);

  int
	web_browser(
		const char *		mpszStepName,
		...);

  int
	web_control(
		const char *		mpszStepName,
		...);

  int
	web_set_rts_key(
		const char *		mpszArg1,
		...);

  int
	web_save_param_length(
		const char * 		mpszParamName,
		...);

  int
	web_save_timestamp_param(
		const char * 		mpszParamName,
		...);

  int
	web_load_cache(
		const char *		mpszStepName,
		...);							 
										 

  int
	web_dump_cache(
		const char *		mpszStepName,
		...);							 
										 
										 

  int
	web_reg_find_in_log(
		const char *		mpszArg1,
		...);							 
										 
										 

  int
	web_get_sockets_info(
		const char *		mpszArg1,
		...);							 
										 
										 
										 
										 

  int
	web_add_cookie_ex(
		const char *		mpszArg1,
		...);							 
										 
										 
										 

  int
	web_hook_java_script(
		const char *		mpszArg1,
		...);							 
										 
										 
										 

 
 
 
 
 
 
 
 
 
 
 
 
  int
	web_reg_async_attributes(
		const char *		mpszArg,
		...
	);

 
 
 
 
 
 
  int
	web_sync(
		 const char *		mpszArg1,
		 ...
	);

 
 
 
 
  int
	web_stop_async(
		const char *		mpszArg1,
		...
	);

 
 
 
 
 

 
 
 

typedef enum WEB_ASYNC_CB_RC_ENUM_T
{
	WEB_ASYNC_CB_RC_OK,				 

	WEB_ASYNC_CB_RC_ABORT_ASYNC_NOT_ERROR,
	WEB_ASYNC_CB_RC_ABORT_ASYNC_ERROR,
										 
										 
										 
										 
	WEB_ASYNC_CB_RC_ENUM_COUNT
} WEB_ASYNC_CB_RC_ENUM;

 
 
 

typedef enum WEB_CONVERS_CB_CALL_REASON_ENUM_T
{
	WEB_CONVERS_CB_CALL_REASON_BUFFER_RECEIVED,
	WEB_CONVERS_CB_CALL_REASON_END_OF_TASK,

	WEB_CONVERS_CB_CALL_REASON_ENUM_COUNT
} WEB_CONVERS_CB_CALL_REASON_ENUM;

 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 

typedef
int														 
	(*RequestCB_t)();

typedef
int														 
	(*ResponseBodyBufferCB_t)(
		  const char *		aLastBufferStr,
		  int				aLastBufferLen,
		  const char *		aAccumulatedStr,
		  int				aAccumulatedLen,
		  int				aHttpStatusCode);

typedef
int														 
	(*ResponseCB_t)(
		  const char *		aResponseHeadersStr,
		  int				aResponseHeadersLen,
		  const char *		aResponseBodyStr,
		  int				aResponseBodyLen,
		  int				aHttpStatusCode);

typedef
int														 
	(*ResponseHeadersCB_t)(
		  int				aHttpStatusCode,
		  const char *		aAccumulatedHeadersStr,
		  int				aAccumulatedHeadersLen);



 
 
 

typedef enum WEB_CONVERS_UTIL_RC_ENUM_T
{
	WEB_CONVERS_UTIL_RC_OK,
	WEB_CONVERS_UTIL_RC_CONVERS_NOT_FOUND,
	WEB_CONVERS_UTIL_RC_TASK_NOT_FOUND,
	WEB_CONVERS_UTIL_RC_INFO_NOT_FOUND,
	WEB_CONVERS_UTIL_RC_INFO_UNAVIALABLE,
	WEB_CONVERS_UTIL_RC_INVALID_ARGUMENT,

	WEB_CONVERS_UTIL_RC_ENUM_COUNT
} WEB_CONVERS_UTIL_RC_ENUM;

 
 
 

  int					 
	web_util_set_request_url(
		  const char *		aUrlStr);

  int					 
	web_util_set_request_body(
		  const char *		aRequestBodyStr);

  int					 
	web_util_set_formatted_request_body(
		  const char *		aRequestBodyStr);


 
 
 
 
 

 
 
 
 
 

 
 
 
 
 
 
 
 

 
 
  int
web_websocket_connect(
		 const char *	mpszArg1,
		 ...
		 );


 
 
 
 
 																						
  int
web_websocket_send(
	   const char *		mpszArg1,
		...
	   );

 
 
 
 
 
 
  int
web_websocket_close(
		const char *	mpszArg1,
		...
		);

 
typedef
void														
(*OnOpen_t)(
			  const char* connectionID,  
			  const char * responseHeader,  
			  int length  
);

typedef
void														
(*OnMessage_t)(
	  const char* connectionID,  
	  int isbinary,  
	  const char * data,  
	  int length  
	);

typedef
void														
(*OnError_t)(
	  const char* connectionID,  
	  const char * message,  
	  int length  
	);

typedef
void														
(*OnClose_t)(
	  const char* connectionID,  
	  int isClosedByClient,  
	  int code,  
	  const char* reason,  
	  int length  
	 );
 
 
 
 
 





# 7 "globals.h" 2

# 1 "lrw_custom_body.h" 1
 




# 8 "globals.h" 2



 
 



# 3 "c:\\users\\pon\\documents\\vugen\\scripts\\testroad\\\\combined_testroad.c" 2

# 1 "vuser_init.c" 1
vuser_init()
{
	return 0;
}
# 4 "c:\\users\\pon\\documents\\vugen\\scripts\\testroad\\\\combined_testroad.c" 2

# 1 "Action.c" 1
Action()
{

	lr_start_transaction("TC001_01_OpenHomePage");

	web_add_cookie("_gid=GA1.2.1213495712.1549427284; DOMAIN=www.testautomationguru.com");

	web_add_cookie("_ga=GA1.2.1371860207.1549427271; DOMAIN=www.testautomationguru.com");

	web_add_cookie("_mailmunch_visitor_id=ddbdf379-8c90-47b0-91df-de883138d4dd; DOMAIN=www.testautomationguru.com");

	web_add_cookie("mailmunch_shown_203533=true; DOMAIN=www.testautomationguru.com");

	web_add_cookie("mailmunch_second_pageview=true; DOMAIN=www.testautomationguru.com");

	web_add_cookie("_mailmunch_seen_month=true; DOMAIN=www.testautomationguru.com");

 
	web_reg_save_param_regexp(
		"ParamName=client",
		"RegExp=\\ \\ \\ \\ \\ data-ad-client=\"(.*?)\"\\\r\\\n\\ \\ \\ \\ \\ data-ad-slot",
		"SEARCH_FILTERS",
		"Scope=Body",
		"IgnoreRedirections=No",
		"RequestUrl=*/www.testautomationguru.com/*",
		"LAST");

	web_url("www.testautomationguru.com", 
		"URL={url}/", 
		"TargetFrame=", 
		"Resource=0", 
		"RecContentType=text/html", 
		"Referer=", 
		"Snapshot=t9.inf", 
		"Mode=HTML", 
		"LAST");

	web_set_sockets_option("SSL_VERSION", "TLS1.2");

	web_add_header("Origin", 
		"{url}");

	web_url("json", 
		"URL=http://ip-api.com/json", 
		"TargetFrame=", 
		"Resource=0", 
		"RecContentType=application/json", 
		"Referer=http://www.testautomationguru.com/", 
		"Snapshot=t10.inf", 
		"Mode=HTML", 
		"LAST");

	web_add_header("X-Requested-With", 
		"XMLHttpRequest");

	web_url("admin-ajax.php", 
		"URL={url}/wp-admin/admin-ajax.php?action=visitor_map_ajax_function&resp%5Bas%5D=AS132032+DTAC+BROADBAND+CO.%2CLTD.&resp%5Bcity%5D=Bangkok&resp%5Bcountry%5D=Thailand&resp%5BcountryCode%5D=TH&resp%5Bisp%5D=DBB-GN&resp%5Blat%5D=13.7833&resp%5Blon%5D=100.5167&resp%5Borg%5D=&resp%5Bquery%5D=103.5.25.7&resp%5Bregion%5D=10&resp%5BregionName%5D=Bangkok&resp%5Bstatus%5D=success&resp%5Btimezone%5D=Asia%2FBangkok&resp%5Bzip%5D=10310&url=%2F", 
		"TargetFrame=", 
		"Resource=0", 
		"RecContentType=text/html", 
		"Referer=http://www.testautomationguru.com/", 
		"Snapshot=t11.inf", 
		"Mode=HTML", 
		"LAST");

	web_add_auto_header("Origin", 
		"{url}");

	web_url("156995", 
		"URL=http://forms.mailmunch.co/sites/156995?visitor_id=ddbdf379-8c90-47b0-91df-de883138d4dd", 
		"TargetFrame=", 
		"Resource=0", 
		"RecContentType=application/json", 
		"Referer=http://www.testautomationguru.com/", 
		"Snapshot=t12.inf", 
		"Mode=HTML", 
		"LAST");

	(web_remove_auto_header("Origin", "ImplicitGen=Yes", "LAST"));

	web_url("settings-1549347575.json", 
		"URL=http://a.mailmunch.co/forms-cache/156995/settings-1549347575.json", 
		"TargetFrame=", 
		"Resource=0", 
		"RecContentType=application/json", 
		"Referer=http://www.testautomationguru.com/", 
		"Snapshot=t13.inf", 
		"Mode=HTML", 
		"LAST");

	web_add_header("Origin", 
		"{url}");

	web_url("index-1530067185.html", 
		"URL=http://a.mailmunch.co/forms-cache/156995/203533/index-1530067185.html", 
		"TargetFrame=", 
		"Resource=0", 
		"RecContentType=text/html", 
		"Referer=http://www.testautomationguru.com/", 
		"Snapshot=t14.inf", 
		"Mode=HTML", 
		"LAST");

	web_add_cookie("IDE=AHWqTUnmSxRCUIr5QRvuzE3_RmHrrOXboTEjUZPl2NcOrG4idfAn-VsPpDt5_xK9; DOMAIN=googleads.g.doubleclick.net");

	web_add_cookie("DSID=NO_DATA; DOMAIN=googleads.g.doubleclick.net");

	web_url("zrt_lookup.html", 
		"URL=https://googleads.g.doubleclick.net/pagead/html/r20190204/r20190131/zrt_lookup.html", 
		"TargetFrame=", 
		"Resource=0", 
		"RecContentType=text/html", 
		"Referer=http://www.testautomationguru.com/", 
		"Snapshot=t15.inf", 
		"Mode=HTML", 
		"LAST");

	web_url("ads",
		"URL=https://googleads.g.doubleclick.net/pagead/ads?client={client}&output=html&h=250&slotname=8887149706&adk=1275891984&adf=3622900839&w=310&fwrn=4&fwrnh=100&lmt=1549427560&rafmt=1&guci=2.2.0.0.2.2.0.0&format=310x250&url=http%3A%2F%2Fwww.testautomationguru.com%2F&flash=32.0.0&fwr=0&resp_fmts=3&wgl=1&dt=1549427560496&bpp=88&bdt=8733&fdt=415&idt=382&shv=r20190204&cbv=r20190131&saldr=aa&abxe=1&correlator=7942659401207&frm=20&pv=2&ga_vid=1213495712.1549427284&ga_sid=1549427561&ga_hid=1267693436&ga_fc=0&icsg=1108491397758972&dssz=37&mdo=0&mso=0&u_tz=420&u_his=1&u_java=1&u_h=720&u_w=1280&u_ah=680&u_aw=1280&u_cd=24&u_nplug=1&u_nmime=2&adx=865&ady=1039&biw=1280&bih=607&scr_x=0&scr_y=0&eid=21060853%2C21062572%2C410075101&oid=3&rx=0&eae=0&fc=656&docm=11&brdim=0%2C73%2C-7%2C-7%2C1280%2C%2C1295%2C695%2C1280%2C607&vis=1&rsz=o%7Co%7CeEbr%7C&abl=NS&ppjl=f&pfx=0&fu=144&bc=1&ifi=1&uci=1.5ly1bvc3nb8x&xpc=U3boNcZGW0&p=http%3A//www.testautomationguru.com&dtd=520",
		"TargetFrame=",
		"Resource=0",
		"RecContentType=text/html",
		"Referer=http://www.testautomationguru.com/",
		"Snapshot=t16.inf",
		"Mode=HTML",
		"LAST");

	web_url("adview",
		"URL=https://googleads.g.doubleclick.net/pagead/adview?ai=CiQvxeWNaXLaCG8KGwgPWrbjIArS4o5NViuXsj4sIkIOFngsQASCi4f1oYP3Nm4aIIKABlu-o_gPIAQOoAwHIA8kEqgTNAU_Q5UtMGtUTpZebvsgCNDeQnciPu3UKGuFgYHdErHcmHsdAOm07G4QGkVjywmeL2UsI07OhdbAin1h31Sp5B8XjSngZqj39j_GPxr6o5_UUAnlHFN9luXN7sck71FB0IC7RDV8uuYq4e845_zLnQyRIGjjedHn6Svi0MUiBRWkkXokPmys5kvuhBf9hww2_rndcqWv20RsgjFeItpwDFMbgKUemKq-UCdxfNTBR6iRK2k9qA3M51XXAcCXl6rwiVKhmNFA2LAN4SjLkvazABKusjfT2AZIFBAgEGAGSBQQIBRgEoAYDgAe8q-SOAagHjs4bqAfVyRuoB6gGqAfZyxuoB8_MG6gHpr4b2AcB8gcEEP7VDdIIBwiAYRABGAKACgHYEwI&sigh=3B_43vSF56A&tpd=AGWhJmvIECZCTJS8aeMxFy0_JAHVGeIDxU37zRs6YgNES8kOVw",
		"TargetFrame=",
		"Resource=0",
		"RecContentType=text/html",
		"Referer=https://googleads.g.doubleclick.net/pagead/ads?client={client}&output=html&h=250&slotname=8887149706&adk=1275891984&adf=3622900839&w=310&fwrn=4&fwrnh=100&lmt=1549427560&rafmt=1&guci=2.2.0.0.2.2.0.0&format=310x250&url=http%3A%2F%2Fwww.testautomationguru.com%2F&flash=32.0.0&fwr=0&resp_fmts=3&wgl=1&dt=1549427560496&bpp=88&bdt=8733&fdt=415&idt=382&shv=r20190204&cbv=r20190131&saldr=aa&abxe=1&correlator=7942659401207&frm=20&pv=2&ga_vid=1213495712.1549427284&ga_sid=1549427561&ga_hid=1267693436&ga_fc=0&icsg=1108491397758972&dssz=37&mdo=0&mso=0&u_tz=420&u_his=1&u_java=1&u_h=720&u_w=1280&u_ah=680&u_aw=1280&u_cd=24&u_nplug=1&u_nmime=2&adx=865&ady=1039&biw=1280&bih=607&scr_x=0&scr_y=0&eid=21060853%2C21062572%2C410075101&oid=3&rx=0&eae=0&fc=656&docm=11&brdim=0%2C73%2C-7%2C-7%2C1280%2C%2C1295%2C695%2C1280%2C607&vis=1&rsz=o%7Co%7CeEbr%7C&abl=NS&ppjl=f&pfx=0&fu=144&bc=1&ifi=1&uci=1.5ly1bvc3nb8x&xpc=U3boNcZGW0&p=http%3A//www.testautomationguru.com&dtd=520",
		"Snapshot=t17.inf",
		"Mode=HTML",
		"LAST");

	web_add_cookie("fr=0VdQKJ9ld2lslsaJA..BcWmDc...1.0.BcWmDc.; DOMAIN=web.facebook.com");

	web_url("like.php", 
		"URL=https://web.facebook.com/plugins/like.php?href=https%3A%2F%2Fwww.facebook.com%2Ftestautomationguru%2F%3Fhc_ref%3DARSJUE0PhuJToeqE6Q71Qh_xY9DGtzNT712ul5EqJVJ2ktLgJGOrxEGHDvCuJE_l1MY%26fref%3Dnf&width=122&layout=button&action=like&size=large&show_faces=true&share=true&height=65&appId&_rdc=1&_rdr", 
		"TargetFrame=", 
		"Resource=0", 
		"RecContentType=text/html", 
		"Referer=https://www.facebook.com/plugins/like.php?href=https%3A%2F%2Fwww.facebook.com%2Ftestautomationguru%2F%3Fhc_ref%3DARSJUE0PhuJToeqE6Q71Qh_xY9DGtzNT712ul5EqJVJ2ktLgJGOrxEGHDvCuJE_l1MY%26fref%3Dnf&width=122&layout=button&action=like&size=large&show_faces=true&share=true&height=65&appId", 
		"Snapshot=t18.inf", 
		"Mode=HTML", 
		"LAST");

	lr_end_transaction("TC001_01_OpenHomePage",2);

	lr_think_time(10);

	lr_start_transaction("TC001_02_SelectSelenium");

	web_url("selenium", 
		"URL={url}/selenium/", 
		"TargetFrame=", 
		"Resource=0", 
		"RecContentType=text/html", 
		"Referer=http://www.testautomationguru.com/", 
		"Snapshot=t19.inf", 
		"Mode=HTML", 
		"LAST");

	web_url("zrt_lookup.html_2", 
		"URL=https://googleads.g.doubleclick.net/pagead/html/r20190204/r20190131/zrt_lookup.html", 
		"TargetFrame=", 
		"Resource=0", 
		"RecContentType=text/html", 
		"Referer=http://www.testautomationguru.com/selenium/", 
		"Snapshot=t20.inf", 
		"Mode=HTML", 
		"LAST");

	web_url("ads_2",
		"URL=https://googleads.g.doubleclick.net/pagead/ads?client={client}&output=html&h=90&slotname=8887149706&adk=1248861733&adf=1549719040&w=1020&fwrn=4&fwrnh=100&lmt=1549427626&rafmt=1&guci=2.2.0.0.2.2.0.0&format=1020x90&url=http%3A%2F%2Fwww.testautomationguru.com%2Fselenium%2F&flash=32.0.0&fwr=0&resp_fmts=3&wgl=1&dt=1549427626163&bpp=42&bdt=645&fdt=136&idt=109&shv=r20190204&cbv=r20190131&saldr=aa&abxe=1&correlator=7096801555006&frm=20&pv=2&ga_vid=1213495712.1549427284&ga_sid=1549427626&ga_hid=714536060&ga_fc=0&icsg=42975915&dssz=21&mdo=0&mso=0&u_tz=420&u_his=2&u_java=1&u_h=720&u_w=1280&u_ah=680&u_aw=1280&u_cd=24&u_nplug=1&u_nmime=2&adx=130&ady=418&biw=1280&bih=607&scr_x=0&scr_y=0&eid=21060853%2C410075101&oid=3&ref=http%3A%2F%2Fwww.testautomationguru.com%2F&rx=0&eae=0&fc=656&docm=11&brdim=0%2C73%2C-7%2C-7%2C1280%2C%2C1295%2C695%2C1280%2C607&vis=1&rsz=%7C%7CeE%7C&abl=CS&ppjl=f&pfx=0&fu=144&bc=1&ifi=1&uci=1.ac13hss03a8&xpc=WEJiMtUGuG&p=http%3A//www.testautomationguru.com&dtd=198",
		"TargetFrame=",
		"Resource=0",
		"RecContentType=text/html",
		"Referer=http://www.testautomationguru.com/selenium/",
		"Snapshot=t21.inf",
		"Mode=HTML",
		"LAST");

	web_url("ads_3",
		"URL=https://googleads.g.doubleclick.net/pagead/ads?client={client}&output=html&h=90&slotname=8887149706&adk=2366443721&adf=10715234&w=1020&fwrn=4&fwrnh=100&lmt=1549427626&rafmt=1&guci=2.2.0.0.2.2.0.0&format=1020x90&url=http%3A%2F%2Fwww.testautomationguru.com%2Fselenium%2F&flash=32.0.0&fwr=0&resp_fmts=3&wgl=1&dt=1549427626205&bpp=25&bdt=689&fdt=435&idt=67&shv=r20190204&cbv=r20190131&saldr=aa&abxe=1&prev_fmts=1020x90&correlator=7096801555006&frm=20&pv=1&ga_vid=1213495712.1549427284&ga_sid=1549427626&ga_hid=714536060&ga_fc=0&icsg=4436164614291452&dssz=40&mdo=0&mso=0&u_tz=420&u_his=2&u_java=1&u_h=720&u_w=1280&u_ah=680&u_aw=1280&u_cd=24&u_nplug=1&u_nmime=2&adx=130&ady=2523&biw=1280&bih=607&scr_x=0&scr_y=0&eid=21060853%2C410075101&oid=3&ref=http%3A%2F%2Fwww.testautomationguru.com%2F&rx=0&eae=0&fc=656&docm=11&brdim=0%2C73%2C-7%2C-7%2C1280%2C%2C1295%2C695%2C1280%2C607&vis=1&rsz=%7C%7CeEbr%7C&abl=CS&ppjl=f&pfx=0&fu=144&bc=1&ifi=2&uci=2.krk9mcgwbu8m&xpc=nJCrnoUSAU&p=http%3A//www.testautomationguru.com&dtd=514",
		"TargetFrame=",
		"Resource=0",
		"RecContentType=text/html",
		"Referer=http://www.testautomationguru.com/selenium/",
		"Snapshot=t22.inf",
		"Mode=HTML",
		"LAST");

	web_add_cookie("fr=0VdQKJ9ld2lslsaJA..BcWmDc...1.0.BcWmDc.; DOMAIN=www.facebook.com");

	web_url("like.php_2", 
		"URL=https://www.facebook.com/plugins/like.php?href=https%3A%2F%2Fwww.facebook.com%2Ftestautomationguru%2F%3Fhc_ref%3DARSJUE0PhuJToeqE6Q71Qh_xY9DGtzNT712ul5EqJVJ2ktLgJGOrxEGHDvCuJE_l1MY%26fref%3Dnf&width=122&layout=button&action=like&size=large&show_faces=true&share=true&height=65&appId", 
		"TargetFrame=", 
		"Resource=0", 
		"RecContentType=text/html", 
		"Referer=http://www.testautomationguru.com/selenium/", 
		"Snapshot=t23.inf", 
		"Mode=HTML", 
		"LAST");

	web_url("adview_2",
		"URL=https://googleads.g.doubleclick.net/pagead/adview?ai=C9JYGqmNaXN_AGI-FwgOLuoPoA4Gu3pNV3IrYvr4Ih_Ge_OIMEAEgouH9aGD9zZuGiCCgAe_1jtsDyAECqAMByAPJBKoE1QFP0OcRwhGFtH8soW8U6yHitsghKqIiKNfyRgiKfMT-St3ZHUBq-V1SAzupiW3pqLDfZzvbQGcplBuJxs7WpNt5bSct8uPXjQq7bRUL4hoQ3MD6RRwnqpT_hUGYra88R0tnRxy3hoBBHqpIr9nhURW_CYy9BJB9kKnK-rYBqzyjzEM8hbh6xfMDQ3V7S6JHCf_UOKKiegGGNyDLPVGV4P_h1vjUaIETPByy-ElQNEhF6vhwe9UPw-kZAtL-hIKVAihQXYLjC2YOt-2X5Qbwk1LJoX8uxxHABMLv4dm5AZIFBAgEGAGSBQQIBRgEkgUECAUYGJIFBQgFGKgBoAYCgAeStd0mqAeOzhuoB9XJG6gHqAaoB9nLG6gHz8wbqAemvhvYBwHyBwQQsLQS0ggHCIBhEAEYAoAKAdgTDA&sigh=trewYh-RJ2E&tpd=AGWhJmvsNVbyKLAhsKB8ffGhuTDKGF9-Wmb4fH8Dln-7Ak_lMg",
		"TargetFrame=",
		"Resource=0",
		"RecContentType=text/html",
		"Referer=https://googleads.g.doubleclick.net/pagead/ads?client={client}&output=html&h=90&slotname=8887149706&adk=2366443721&adf=10715234&w=1020&fwrn=4&fwrnh=100&lmt=1549427626&rafmt=1&guci=2.2.0.0.2.2.0.0&format=1020x90&url=http%3A%2F%2Fwww.testautomationguru.com%2Fselenium%2F&flash=32.0.0&fwr=0&resp_fmts=3&wgl=1&dt=1549427626205&bpp=25&bdt=689&fdt=435&idt=67&shv=r20190204&cbv=r20190131&saldr=aa&abxe=1&prev_fmts=1020x90&correlator=7096801555006&frm=20&pv=1&ga_vid=1213495712.1549427284&ga_sid=1549427626&ga_hid=714536060&ga_fc=0&icsg=4436164614291452&dssz=40&mdo=0&mso=0&u_tz=420&u_his=2&u_java=1&u_h=720&u_w=1280&u_ah=680&u_aw=1280&u_cd=24&u_nplug=1&u_nmime=2&adx=130&ady=2523&biw=1280&bih=607&scr_x=0&scr_y=0&eid=21060853%2C410075101&oid=3&ref=http%3A%2F%2Fwww.testautomationguru.com%2F&rx=0&eae=0&fc=656&docm=11&brdim=0%2C73%2C-7%2C-7%2C1280%2C%2C1295%2C695%2C1280%2C607&vis=1&rsz=%7C%7CeEbr%7C&abl=CS&ppjl=f&pfx=0&fu=144&bc=1&ifi=2&uci=2.krk9mcgwbu8m&xpc=nJCrnoUSAU&p=http%3A//www.testautomationguru.com&dtd=514",
		"Snapshot=t24.inf",
		"Mode=HTML",
		"LAST");

	web_url("like.php_3", 
		"URL=https://web.facebook.com/plugins/like.php?href=https%3A%2F%2Fwww.facebook.com%2Ftestautomationguru%2F%3Fhc_ref%3DARSJUE0PhuJToeqE6Q71Qh_xY9DGtzNT712ul5EqJVJ2ktLgJGOrxEGHDvCuJE_l1MY%26fref%3Dnf&width=122&layout=button&action=like&size=large&show_faces=true&share=true&height=65&appId&_rdc=1&_rdr", 
		"TargetFrame=", 
		"Resource=0", 
		"RecContentType=text/html", 
		"Referer=https://www.facebook.com/plugins/like.php?href=https%3A%2F%2Fwww.facebook.com%2Ftestautomationguru%2F%3Fhc_ref%3DARSJUE0PhuJToeqE6Q71Qh_xY9DGtzNT712ul5EqJVJ2ktLgJGOrxEGHDvCuJE_l1MY%26fref%3Dnf&width=122&layout=button&action=like&size=large&show_faces=true&share=true&height=65&appId", 
		"Snapshot=t25.inf", 
		"Mode=HTML", 
		"LAST");

	web_add_header("X-Requested-With", 
		"XMLHttpRequest");

	web_url("admin-ajax.php_2", 
		"URL={url}/wp-admin/admin-ajax.php?action=visitor_map_ajax_function&resp%5Bas%5D=AS132032+DTAC+BROADBAND+CO.%2CLTD.&resp%5Bcity%5D=Bangkok&resp%5Bcountry%5D=Thailand&resp%5BcountryCode%5D=TH&resp%5Bisp%5D=DBB-GN&resp%5Blat%5D=13.7833&resp%5Blon%5D=100.5167&resp%5Borg%5D=&resp%5Bquery%5D=103.5.25.7&resp%5Bregion%5D=10&resp%5BregionName%5D=Bangkok&resp%5Bstatus%5D=success&resp%5Btimezone%5D=Asia%2FBangkok&resp%5Bzip%5D=10310&url=%2Fselenium%2F", 
		"TargetFrame=", 
		"Resource=0", 
		"RecContentType=text/html", 
		"Referer=http://www.testautomationguru.com/selenium/", 
		"Snapshot=t26.inf", 
		"Mode=HTML", 
		"LAST");

	web_url("adview_3",
		"URL=https://googleads.g.doubleclick.net/pagead/adview?ai=CwqmBqmNaXLrOApiHwgOyk7GwDaamrf1U1eiaqr8IwI23ARABIKLh_Whg_c2bhoggoAG7h9m6A8gBAakCJOaPJdXPiT6oAwHIA8MEqgTRAU_QKDjGPyQd87KoYfp33zRTO5CeQEkgsyeQO0zWG_7-lDLbEpzscQ9r97aj9ne12_fgOnf1EpN8NUrBudo4bylbdDELg1YoxtCP17YchSn_KIKfJwG_qN99zONuoUDPpErYehyvNGmOBPH564n9UhAKx5YrgdmimqX8aSNxI8jhPNUTUXXAPnbgJXCZVqNULctuTkxK2RVZHTTB37NdQAWJRN6m_ff_w8-xpcqi2wncn4SQYclyUE12Mvn5RVJEcU2pT3ekmV85fjzJUJsevZ-IwASv-6XG8AGSBQQIBBgBkgUECAUYBKAGUYAHrfimRagHjs4bqAfVyRuoB6gGqAfZyxuoB8_MG6gHpr4b2AcB8gcFENbWggLSCAcIgGEQARgCgAoB2BMNiBQD&sigh=IlQB0ghF6Nc&tpd=AGWhJmtB4j2ysV4YAbOJkbIPeaex7YHMNnovk2pX7sCP9Trcmg",
		"TargetFrame=",
		"Resource=0",
		"RecContentType=text/html",
		"Referer=https://googleads.g.doubleclick.net/pagead/ads?client={client}&output=html&h=90&slotname=8887149706&adk=1248861733&adf=1549719040&w=1020&fwrn=4&fwrnh=100&lmt=1549427626&rafmt=1&guci=2.2.0.0.2.2.0.0&format=1020x90&url=http%3A%2F%2Fwww.testautomationguru.com%2Fselenium%2F&flash=32.0.0&fwr=0&resp_fmts=3&wgl=1&dt=1549427626163&bpp=42&bdt=645&fdt=136&idt=109&shv=r20190204&cbv=r20190131&saldr=aa&abxe=1&correlator=7096801555006&frm=20&pv=2&ga_vid=1213495712.1549427284&ga_sid=1549427626&ga_hid=714536060&ga_fc=0&icsg=42975915&dssz=21&mdo=0&mso=0&u_tz=420&u_his=2&u_java=1&u_h=720&u_w=1280&u_ah=680&u_aw=1280&u_cd=24&u_nplug=1&u_nmime=2&adx=130&ady=418&biw=1280&bih=607&scr_x=0&scr_y=0&eid=21060853%2C410075101&oid=3&ref=http%3A%2F%2Fwww.testautomationguru.com%2F&rx=0&eae=0&fc=656&docm=11&brdim=0%2C73%2C-7%2C-7%2C1280%2C%2C1295%2C695%2C1280%2C607&vis=1&rsz=%7C%7CeE%7C&abl=CS&ppjl=f&pfx=0&fu=144&bc=1&ifi=1&uci=1.ac13hss03a8&xpc=WEJiMtUGuG&p=http%3A//www.testautomationguru.com&dtd=198",
		"Snapshot=t27.inf",
		"Mode=HTML",
		"LAST");

	lr_end_transaction("TC001_02_SelectSelenium",2);

	lr_think_time(10);

	lr_start_transaction("TC001_03_SelectJemeter");

	web_url("jmeter", 
		"URL={url}/jmeter/", 
		"TargetFrame=", 
		"Resource=0", 
		"RecContentType=text/html", 
		"Referer=http://www.testautomationguru.com/selenium/", 
		"Snapshot=t28.inf", 
		"Mode=HTML", 
		"LAST");

	web_url("zrt_lookup.html_3", 
		"URL=https://googleads.g.doubleclick.net/pagead/html/r20190204/r20190131/zrt_lookup.html", 
		"TargetFrame=", 
		"Resource=0", 
		"RecContentType=text/html", 
		"Referer=http://www.testautomationguru.com/jmeter/", 
		"Snapshot=t29.inf", 
		"Mode=HTML", 
		"LAST");

	web_url("ads_4",
		"URL=https://googleads.g.doubleclick.net/pagead/ads?client={client}&output=html&h=90&slotname=8887149706&adk=1248861733&adf=1549719040&w=1020&fwrn=4&fwrnh=100&lmt=1549427651&rafmt=1&guci=2.2.0.0.2.2.0.0&format=1020x90&url=http%3A%2F%2Fwww.testautomationguru.com%2Fjmeter%2F&flash=32.0.0&fwr=0&resp_fmts=3&wgl=1&dt=1549427651670&bpp=47&bdt=630&fdt=141&idt=112&shv=r20190204&cbv=r20190131&saldr=aa&abxe=1&correlator=4427604344244&frm=20&pv=2&ga_vid=1213495712.1549427284&ga_sid=1549427652&ga_hid=495803905&ga_fc=0&icsg=42975915&dssz=21&mdo=0&mso=0&u_tz=420&u_his=3&u_java=1&u_h=720&u_w=1280&u_ah=680&u_aw=1280&u_cd=24&u_nplug=1&u_nmime=2&adx=130&ady=418&biw=1280&bih=607&scr_x=0&scr_y=0&eid=21060853%2C410075101&oid=3&ref=http%3A%2F%2Fwww.testautomationguru.com%2Fselenium%2F&rx=0&eae=0&fc=656&docm=11&brdim=0%2C73%2C-7%2C-7%2C1280%2C%2C1295%2C695%2C1280%2C607&vis=1&rsz=%7C%7CeE%7C&abl=CS&ppjl=f&pfx=0&fu=144&bc=1&ifi=1&uci=1.ijb88rttygd5&xpc=Xp6x5M47ft&p=http%3A//www.testautomationguru.com&dtd=203",
		"TargetFrame=",
		"Resource=0",
		"RecContentType=text/html",
		"Referer=http://www.testautomationguru.com/jmeter/",
		"Snapshot=t30.inf",
		"Mode=HTML",
		"LAST");

	web_url("ads_5",
		"URL=https://googleads.g.doubleclick.net/pagead/ads?client={client}&output=html&h=90&slotname=8887149706&adk=2366443721&adf=10715234&w=1020&fwrn=4&fwrnh=100&lmt=1549427652&rafmt=1&guci=2.2.0.0.2.2.0.0&format=1020x90&url=http%3A%2F%2Fwww.testautomationguru.com%2Fjmeter%2F&flash=32.0.0&fwr=0&resp_fmts=3&wgl=1&dt=1549427651717&bpp=25&bdt=675&fdt=307&idt=65&shv=r20190204&cbv=r20190131&saldr=aa&abxe=1&prev_fmts=1020x90&correlator=4427604344244&frm=20&pv=1&ga_vid=1213495712.1549427284&ga_sid=1549427652&ga_hid=495803905&ga_fc=0&icsg=4436164614291452&dssz=41&mdo=0&mso=0&u_tz=420&u_his=3&u_java=1&u_h=720&u_w=1280&u_ah=680&u_aw=1280&u_cd=24&u_nplug=1&u_nmime=2&adx=130&ady=2071&biw=1280&bih=607&scr_x=0&scr_y=0&eid=21060853%2C410075101&oid=3&ref=http%3A%2F%2Fwww.testautomationguru.com%2Fselenium%2F&rx=0&eae=0&fc=656&docm=11&brdim=0%2C73%2C-7%2C-7%2C1280%2C%2C1295%2C695%2C1280%2C607&vis=1&rsz=%7C%7CeEbr%7C&abl=CS&ppjl=f&pfx=0&fu=144&bc=1&ifi=2&uci=2.3jg096370tdv&xpc=R6Nfdca6fY&p=http%3A//www.testautomationguru.com&dtd=356"
		"",
		"TargetFrame=",
		"Resource=0",
		"RecContentType=text/html",
		"Referer=http://www.testautomationguru.com/jmeter/",
		"Snapshot=t31.inf",
		"Mode=HTML",
		"LAST");

	web_url("like.php_4", 
		"URL=https://www.facebook.com/plugins/like.php?href=https%3A%2F%2Fwww.facebook.com%2Ftestautomationguru%2F%3Fhc_ref%3DARSJUE0PhuJToeqE6Q71Qh_xY9DGtzNT712ul5EqJVJ2ktLgJGOrxEGHDvCuJE_l1MY%26fref%3Dnf&width=122&layout=button&action=like&size=large&show_faces=true&share=true&height=65&appId", 
		"TargetFrame=", 
		"Resource=0", 
		"RecContentType=text/html", 
		"Referer=http://www.testautomationguru.com/jmeter/", 
		"Snapshot=t32.inf", 
		"Mode=HTML", 
		"LAST");

	web_url("adview_4",
		"URL=https://googleads.g.doubleclick.net/pagead/adview?ai=CNnmFw2NaXIKMLs-PvgSItKiIDIGu3pNVjIXYvr4Ih_Ge_OIMEAEgouH9aGD9zZuGiCCgAe_1jtsDyAECqAMByAPJBKoEzAFP0G198I1pAa9jRUmsZLyWVcfZybZnGzk_vVZAPJp7l4IRkIkBzYsfD0-37LXHWcFJ2VN-3MGI4JCy9HgYS8t8DGyb6p0j10mJPipLrAB_gM2v9tZvJ_ouaVPeudj3PxqowuO610YWMXZ4ZtfmhKiVWfCkn98wBPg1SiMwvOCgIcralSVKv6svFAZ-4EKOD6YmdKYMF0ZpipTBFmjhzg-2nq7R38w6gazgcSh1G-u9zZhR_DkyeIeJx9vV70md5jcgnPVuL0goTUgpzOzABMLv4dm5AZIFBAgEGAGSBQQIBRgEkgUECAUYGJIFBQgFGKgBoAYCgAeStd0mqAeOzhuoB9XJG6gHqAaoB9nLG6gHz8wbqAemvhvYBwHyBwQQoqQK0ggHCIBhEAEYAoAKAdgTDA&sigh=YDXZq0GBGG4&tpd=AGWhJmtMaHjstUfJys7vVUgMi1VHGaqiKfGs8MZq496BgLc-Ew",
		"TargetFrame=",
		"Resource=0",
		"RecContentType=text/html",
		"Referer=https://googleads.g.doubleclick.net/pagead/ads?client={client}&output=html&h=90&slotname=8887149706&adk=2366443721&adf=10715234&w=1020&fwrn=4&fwrnh=100&lmt=1549427652&rafmt=1&guci=2.2.0.0.2.2.0.0&format=1020x90&url=http%3A%2F%2Fwww.testautomationguru.com%2Fjmeter%2F&flash=32.0.0&fwr=0&resp_fmts=3&wgl=1&dt=1549427651717&bpp=25&bdt=675&fdt=307&idt=65&shv=r20190204&cbv=r20190131&saldr=aa&abxe=1&prev_fmts=1020x90&correlator=4427604344244&frm=20&pv=1&ga_vid=1213495712.1549427284&ga_sid=1549427652&ga_hid=495803905&ga_fc=0&icsg=4436164614291452&dssz=41&mdo=0&mso=0&u_tz=420&u_his=3&u_java=1&u_h=720&u_w=1280&u_ah=680&u_aw=1280&u_cd=24&u_nplug=1&u_nmime=2&adx=130&ady=2071&biw=1280&bih=607&scr_x=0&scr_y=0&eid=21060853%2C410075101&oid=3&ref=http%3A%2F%2Fwww.testautomationguru.com%2Fselenium%2F&rx=0&eae=0&fc=656&docm=11&brdim=0%2C73%2C-7%2C-7%2C1280%2C%2C1295%2C695%2C1280%2C607&vis=1&rsz=%7C%7CeEbr%7C&abl=CS&ppjl=f&pfx=0&fu=144&bc=1&ifi=2&uci=2.3jg096370tdv&xpc=R6Nfdca6fY&p=http%3A//www.testautomationguru.com&dtd"
		"=356",
		"Snapshot=t33.inf",
		"Mode=HTML",
		"LAST");

	web_add_header("X-Requested-With", 
		"XMLHttpRequest");

	web_url("admin-ajax.php_3", 
		"URL={url}/wp-admin/admin-ajax.php?action=visitor_map_ajax_function&resp%5Bas%5D=AS132032+DTAC+BROADBAND+CO.%2CLTD.&resp%5Bcity%5D=Bangkok&resp%5Bcountry%5D=Thailand&resp%5BcountryCode%5D=TH&resp%5Bisp%5D=DBB-GN&resp%5Blat%5D=13.7833&resp%5Blon%5D=100.5167&resp%5Borg%5D=&resp%5Bquery%5D=103.5.25.7&resp%5Bregion%5D=10&resp%5BregionName%5D=Bangkok&resp%5Bstatus%5D=success&resp%5Btimezone%5D=Asia%2FBangkok&resp%5Bzip%5D=10310&url=%2Fjmeter%2F", 
		"TargetFrame=", 
		"Resource=0", 
		"RecContentType=text/html", 
		"Referer=http://www.testautomationguru.com/jmeter/", 
		"Snapshot=t34.inf", 
		"Mode=HTML", 
		"LAST");

	web_url("like.php_5", 
		"URL=https://web.facebook.com/plugins/like.php?href=https%3A%2F%2Fwww.facebook.com%2Ftestautomationguru%2F%3Fhc_ref%3DARSJUE0PhuJToeqE6Q71Qh_xY9DGtzNT712ul5EqJVJ2ktLgJGOrxEGHDvCuJE_l1MY%26fref%3Dnf&width=122&layout=button&action=like&size=large&show_faces=true&share=true&height=65&appId&_rdc=1&_rdr", 
		"TargetFrame=", 
		"Resource=0", 
		"RecContentType=text/html", 
		"Referer=https://www.facebook.com/plugins/like.php?href=https%3A%2F%2Fwww.facebook.com%2Ftestautomationguru%2F%3Fhc_ref%3DARSJUE0PhuJToeqE6Q71Qh_xY9DGtzNT712ul5EqJVJ2ktLgJGOrxEGHDvCuJE_l1MY%26fref%3Dnf&width=122&layout=button&action=like&size=large&show_faces=true&share=true&height=65&appId", 
		"Snapshot=t35.inf", 
		"Mode=HTML", 
		"LAST");

	web_url("adview_5",
		"URL=https://googleads.g.doubleclick.net/pagead/adview?ai=CETfkw2NaXI2wIoryvQSq_5rYDaamrf1UzbTG_8gIwI23ARABIKLh_Whg_c2bhoggoAG7h9m6A8gBAakCJOaPJdXPiT6oAwHIA8MEqgTIAU_Qa_9PwNhfZ_ub3VhaQ5W-ChQ41axQY7NjJil1Rgc-HDmo-N6Q2vHL0Njo2QnjpD7Kv1DQyRuX8wDFrikYK_cLY55runHXZ5B0RuqDnWxbyHAIY8Sd_aKVuI8BVnKbO9kwT0qabCzxX5d7t1GjSQUeocgeRX3qXIrVphKL0etkvpUdfmaHtHoLibk16TveT_73QLg_780A1bflb88lDjZuSChQ6UIv6NKI31ijfXYZuyVvSj-sJxOKXN3TFI6dsF-4YByvFghXwASv-6XG8AGSBQQIBBgBkgUECAUYBKAGUYAHrfimRagHjs4bqAfVyRuoB6gGqAfZyxuoB8_MG6gHpr4b2AcB8gcFEKq-zgHSCAcIgGEQARgCgAoB2BMN&sigh=Rrt4KSzv6-o&tpd=AGWhJmscULKDiaAJW3Fy56aXUrqEn-ekHmMNkSo-9KHL37GOEw",
		"TargetFrame=",
		"Resource=0",
		"RecContentType=text/html",
		"Referer=https://googleads.g.doubleclick.net/pagead/ads?client={client}&output=html&h=90&slotname=8887149706&adk=1248861733&adf=1549719040&w=1020&fwrn=4&fwrnh=100&lmt=1549427651&rafmt=1&guci=2.2.0.0.2.2.0.0&format=1020x90&url=http%3A%2F%2Fwww.testautomationguru.com%2Fjmeter%2F&flash=32.0.0&fwr=0&resp_fmts=3&wgl=1&dt=1549427651670&bpp=47&bdt=630&fdt=141&idt=112&shv=r20190204&cbv=r20190131&saldr=aa&abxe=1&correlator=4427604344244&frm=20&pv=2&ga_vid=1213495712.1549427284&ga_sid=1549427652&ga_hid=495803905&ga_fc=0&icsg=42975915&dssz=21&mdo=0&mso=0&u_tz=420&u_his=3&u_java=1&u_h=720&u_w=1280&u_ah=680&u_aw=1280&u_cd=24&u_nplug=1&u_nmime=2&adx=130&ady=418&biw=1280&bih=607&scr_x=0&scr_y=0&eid=21060853%2C410075101&oid=3&ref=http%3A%2F%2Fwww.testautomationguru.com%2Fselenium%2F&rx=0&eae=0&fc=656&docm=11&brdim=0%2C73%2C-7%2C-7%2C1280%2C%2C1295%2C695%2C1280%2C607&vis=1&rsz=%7C%7CeE%7C&abl=CS&ppjl=f&pfx=0&fu=144&bc=1&ifi=1&uci=1.ijb88rttygd5&xpc=Xp6x5M47ft&p=http%3A//www.testautomationguru.com&dtd=203",
		"Snapshot=t36.inf",
		"Mode=HTML",
		"LAST");

	lr_end_transaction("TC001_03_SelectJemeter",2);

	lr_think_time(10);

	lr_start_transaction("TC001_04_SelectDevops");

	web_url("ci-cd", 
		"URL={url}/category/ci-cd/", 
		"TargetFrame=", 
		"Resource=0", 
		"RecContentType=text/html", 
		"Referer=http://www.testautomationguru.com/jmeter/", 
		"Snapshot=t37.inf", 
		"Mode=HTML", 
		"LAST");

	web_url("like.php_6", 
		"URL=https://www.facebook.com/plugins/like.php?href=https%3A%2F%2Fwww.facebook.com%2Ftestautomationguru%2F%3Fhc_ref%3DARSJUE0PhuJToeqE6Q71Qh_xY9DGtzNT712ul5EqJVJ2ktLgJGOrxEGHDvCuJE_l1MY%26fref%3Dnf&width=122&layout=button&action=like&size=large&show_faces=true&share=true&height=65&appId", 
		"TargetFrame=", 
		"Resource=0", 
		"RecContentType=text/html", 
		"Referer=http://www.testautomationguru.com/category/ci-cd/", 
		"Snapshot=t38.inf", 
		"Mode=HTML", 
		"LAST");

	web_add_header("X-Requested-With", 
		"XMLHttpRequest");

	web_url("admin-ajax.php_4", 
		"URL={url}/wp-admin/admin-ajax.php?action=visitor_map_ajax_function&resp%5Bas%5D=AS132032+DTAC+BROADBAND+CO.%2CLTD.&resp%5Bcity%5D=Bangkok&resp%5Bcountry%5D=Thailand&resp%5BcountryCode%5D=TH&resp%5Bisp%5D=DBB-GN&resp%5Blat%5D=13.7833&resp%5Blon%5D=100.5167&resp%5Borg%5D=&resp%5Bquery%5D=103.5.25.7&resp%5Bregion%5D=10&resp%5BregionName%5D=Bangkok&resp%5Bstatus%5D=success&resp%5Btimezone%5D=Asia%2FBangkok&resp%5Bzip%5D=10310&url=%2Fcategory%2Fci-cd%2F", 
		"TargetFrame=", 
		"Resource=0", 
		"RecContentType=text/html", 
		"Referer=http://www.testautomationguru.com/category/ci-cd/", 
		"Snapshot=t39.inf", 
		"Mode=HTML", 
		"LAST");

	web_url("like.php_7", 
		"URL=https://web.facebook.com/plugins/like.php?href=https%3A%2F%2Fwww.facebook.com%2Ftestautomationguru%2F%3Fhc_ref%3DARSJUE0PhuJToeqE6Q71Qh_xY9DGtzNT712ul5EqJVJ2ktLgJGOrxEGHDvCuJE_l1MY%26fref%3Dnf&width=122&layout=button&action=like&size=large&show_faces=true&share=true&height=65&appId&_rdc=1&_rdr", 
		"TargetFrame=", 
		"Resource=0", 
		"RecContentType=text/html", 
		"Referer=https://www.facebook.com/plugins/like.php?href=https%3A%2F%2Fwww.facebook.com%2Ftestautomationguru%2F%3Fhc_ref%3DARSJUE0PhuJToeqE6Q71Qh_xY9DGtzNT712ul5EqJVJ2ktLgJGOrxEGHDvCuJE_l1MY%26fref%3Dnf&width=122&layout=button&action=like&size=large&show_faces=true&share=true&height=65&appId", 
		"Snapshot=t40.inf", 
		"Mode=HTML", 
		"LAST");

	lr_end_transaction("TC001_04_SelectDevops",2);

	lr_think_time(10);

	lr_start_transaction("TC005_05_SelectDocker");

	web_url("docker", 
		"URL={url}/category/docker/", 
		"TargetFrame=", 
		"Resource=0", 
		"RecContentType=text/html", 
		"Referer=http://www.testautomationguru.com/category/ci-cd/", 
		"Snapshot=t41.inf", 
		"Mode=HTML", 
		"LAST");

	web_url("like.php_8", 
		"URL=https://www.facebook.com/plugins/like.php?href=https%3A%2F%2Fwww.facebook.com%2Ftestautomationguru%2F%3Fhc_ref%3DARSJUE0PhuJToeqE6Q71Qh_xY9DGtzNT712ul5EqJVJ2ktLgJGOrxEGHDvCuJE_l1MY%26fref%3Dnf&width=122&layout=button&action=like&size=large&show_faces=true&share=true&height=65&appId", 
		"TargetFrame=", 
		"Resource=0", 
		"RecContentType=text/html", 
		"Referer=http://www.testautomationguru.com/category/docker/", 
		"Snapshot=t42.inf", 
		"Mode=HTML", 
		"LAST");

	web_url("like.php_9", 
		"URL=https://web.facebook.com/plugins/like.php?href=https%3A%2F%2Fwww.facebook.com%2Ftestautomationguru%2F%3Fhc_ref%3DARSJUE0PhuJToeqE6Q71Qh_xY9DGtzNT712ul5EqJVJ2ktLgJGOrxEGHDvCuJE_l1MY%26fref%3Dnf&width=122&layout=button&action=like&size=large&show_faces=true&share=true&height=65&appId&_rdc=1&_rdr", 
		"TargetFrame=", 
		"Resource=0", 
		"RecContentType=text/html", 
		"Referer=https://www.facebook.com/plugins/like.php?href=https%3A%2F%2Fwww.facebook.com%2Ftestautomationguru%2F%3Fhc_ref%3DARSJUE0PhuJToeqE6Q71Qh_xY9DGtzNT712ul5EqJVJ2ktLgJGOrxEGHDvCuJE_l1MY%26fref%3Dnf&width=122&layout=button&action=like&size=large&show_faces=true&share=true&height=65&appId", 
		"Snapshot=t43.inf", 
		"Mode=HTML", 
		"LAST");

	web_add_header("X-Requested-With", 
		"XMLHttpRequest");

	web_url("admin-ajax.php_5", 
		"URL={url}/wp-admin/admin-ajax.php?action=visitor_map_ajax_function&resp%5Bas%5D=AS132032+DTAC+BROADBAND+CO.%2CLTD.&resp%5Bcity%5D=Bangkok&resp%5Bcountry%5D=Thailand&resp%5BcountryCode%5D=TH&resp%5Bisp%5D=DBB-GN&resp%5Blat%5D=13.7833&resp%5Blon%5D=100.5167&resp%5Borg%5D=&resp%5Bquery%5D=103.5.25.7&resp%5Bregion%5D=10&resp%5BregionName%5D=Bangkok&resp%5Bstatus%5D=success&resp%5Btimezone%5D=Asia%2FBangkok&resp%5Bzip%5D=10310&url=%2Fcategory%2Fdocker%2F", 
		"TargetFrame=", 
		"Resource=0", 
		"RecContentType=text/html", 
		"Referer=http://www.testautomationguru.com/category/docker/", 
		"Snapshot=t44.inf", 
		"Mode=HTML", 
		"LAST");

	lr_end_transaction("TC005_05_SelectDocker",2);

	return 0;
}
# 5 "c:\\users\\pon\\documents\\vugen\\scripts\\testroad\\\\combined_testroad.c" 2

# 1 "vuser_end.c" 1
vuser_end()
{
	return 0;
}
# 6 "c:\\users\\pon\\documents\\vugen\\scripts\\testroad\\\\combined_testroad.c" 2

